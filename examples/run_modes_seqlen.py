#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
@author: T. Guyet, AGROCAMPUS-OUEST
@date: 10/2019

Voisinage
"""

import sys

sys.path.append("../")

import pychronicles.chronicle_python as chropy
import pychronicles.chronicle as chro


import numpy as np
import time


def random_int(pvals):
    v = np.random.uniform()
    s = pvals[0]
    i = 0
    while s < v:
        i += 1
        s += pvals[i]
    return i


if __name__ == "__main__":

    file = open("out.csv", "w")
    file.write(
        "it,nbseq,seqlen,vocsize,varvoc,delay_mean,delay_std,ctconstr,csize,ctsconstr_delay_mean,ctsconstr_delay_std,ctsconstr_duration_std,time1,nb1,time2,nb2,time3,nb3\n"
    )

    # sequence parameters
    nb_sequence = 100
    sequence_length = 200
    sequence_vocsize = 5
    sequence_delai_mean = 4
    sequence_delai_std = 1
    varvoc = 0.8
    # chronicle parameters
    density = 0.4
    csize = np.random.randint(4, 5)  # put 4,5 to impose chonicle of size 4
    trans_delay_mean = sequence_delai_mean
    trans_delay_std = sequence_delai_std
    trans_duration_std = 5 * sequence_delai_mean

    nb_expe = 50
    id_param = 0
    # for varvoc in [0,0.4,0.8,1.6,2]:
    # params=[10000, 5000, 2500, 1250, 625]
    params = [10000, 5000, 2500, 1250, 625]
    # for sequence_vocsize in params:
    for sequence_length in params:
        #    for csize in [3, 4, 5, 6]:
        # for trans_duration_std in [5, 10, 15, 20, 30, 40]:
        # file = open("out_td"+str(int(trans_duration_std))+"_cs"+str(csize)+".csv", "w")
        file = open("out_seqlen_" + str(int(id_param)) + ".csv", "w")
        if id_param == 0:
            file.write(
                "it,compil,nbseq,seqlen,vocsize,varvoc,delay_mean,delay_std,ctconstr,csize,ctsconstr_delay_mean,ctsconstr_delay_std,ctsconstr_duration_std,time1,nb1,time2,nb2,time3,nb3\n"
            )
        print("####### parameter =" + str(params[id_param]) + " #######")
        id_param += 1
        for compil in ["None", "Cython"]:
            for it in range(nb_expe):
                print("---- run expe " + str(it) + "/" + str(nb_expe) + " ----")

                ######################## Random sequence #########################
                print("# generate sequences")
                pvals = [
                    1.0 / float(sequence_vocsize)
                    + (
                        float(varvoc * (i - (sequence_vocsize - 1) / 2))
                        / float(sequence_vocsize - 1)
                    )
                    / float(sequence_vocsize)
                    for i in range(sequence_vocsize)
                ]

                # generate nb_sequence sequences
                seqs = []
                for ids in range(nb_sequence):
                    t = 0
                    seq = []
                    # hist={}
                    for i in range(sequence_length):
                        t = t + np.int(
                            np.random.normal(sequence_delai_mean, sequence_delai_std, 1)
                        )
                        e = random_int(pvals) + 1
                        # hist[e]=hist.setdefault(e,0)+1
                        seq.append((e, t))
                    # print(pvals)
                    # print(seq)
                    # print(hist)
                    seqs.append(chro.reformat_sequence(seq))

                ######################## Random chronicle #########################
                print("# generate chronicle")
                inconsistent = True
                attempt = 0

                while inconsistent and attempt < 20:
                    # generate one random chronicle
                    if compil == "Cython":
                        c = chro.Chronicle()
                    elif compil == "None":
                        c = chropy.Chronicle()
                    print("\t attempt " + str(attempt))

                    for i in range(csize):
                        c.add_event(i, np.random.randint(0, csize) + 1)
                    ctconstr = 0
                    for i in range(csize):
                        for j in range(csize - i):
                            if np.random.uniform() < density:
                                t = np.int(
                                    np.random.normal(
                                        trans_delay_mean, trans_delay_std, 1
                                    )
                                )
                                d = np.abs(
                                    np.int(np.random.normal(0, trans_duration_std, 1))
                                )
                                c.add_constraint(i, i + j + 1, (t, t + d))
                                ctconstr += 1

                    c.minimize()
                    inconsistent = c.inconsistent
                    attempt += 1
                if inconsistent:
                    print("\tinconsistent chronicles ... abording!")
                    continue

                file.write(
                    str(it)
                    + ","
                    + str(compil)
                    + ","
                    + str(nb_sequence)
                    + ","
                    + str(sequence_length)
                    + ","
                    + str(sequence_vocsize)
                    + ","
                    + str(varvoc)
                    + ","
                    + str(sequence_delai_mean)
                    + ","
                    + str(sequence_delai_std)
                    + ","
                    + str(ctconstr)
                    + ","
                    + str(csize)
                    + ","
                    + str(trans_delay_mean)
                    + ","
                    + str(trans_delay_std)
                    + ","
                    + str(trans_duration_std)
                )

                print("# evaluate mode 1")
                start = time.time()
                c.recmode = 1
                nb = 0
                for seq in seqs:
                    occs = c.recognize(seq)
                    nb += len(occs)
                duration_mode1 = time.time() - start
                file.write("," + str(duration_mode1) + "," + str(nb))

                print("# evaluate mode 2")
                c.recmode = 2
                start = time.time()
                nb = 0
                for seq in seqs:
                    occs = c.recognize(seq)
                    nb += len(occs)
                duration_mode2 = time.time() - start
                file.write("," + str(duration_mode2) + "," + str(nb))

                print("# evaluate mode 3")
                c.recmode = 3
                start = time.time()
                nb = 0
                for seq in seqs:
                    occs = c.recognize(seq)
                    nb += len(occs)
                duration_mode3 = time.time() - start
                file.write("," + str(duration_mode3) + "," + str(nb))

                file.write("\n")
                print("# expe ended")

        file.close()
