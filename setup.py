from setuptools import setup

setup(
    name='pychronicles',
    version='0.1.6',
    packages=['pychronicles']
)