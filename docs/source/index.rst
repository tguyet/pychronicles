.. PyChronicles documentation master file


PyChronicles's documentation
===============================

PyChronicles is a Python package that provides an implementation of **chronicles**.
A Chronicle is temporal model that describe a complex temporal arrangement of
events. It can be used to analyse timed sequences in different manners:

* situation recognition: the occurrence of a chronicle can efficiently be enumerated in (long) sequences,
* abstraction: a chronicle can generalize a collection of sequences
* mining: frequent chronicles can be extracted from a set of sequences (*not yet available*)

The PyChronicle package provides tools to manipulate easily such types of models, including with Pandas dataframes.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   readme
   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
