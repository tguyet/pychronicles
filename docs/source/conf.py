# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information
#
# I used the following documentations to help me to create this configuration file
#   * https://shunsvineyard.info/2019/09/19/use-sphinx-for-python-documentation/
#   * https://github.com/readthedocs-examples/example-sphinx-basic/blob/main/docs/conf.py
#   * https://www.sphinx-doc.org/en/master/usage/markdown.html


import os
import sys

sys.path.insert(0, os.path.abspath("../.."))  # location of the sources

import sphinx_rtd_theme

project = "PyChronicles"
copyright = "2023, Thomas Guyet"
author = "Thomas Guyet"
release = "0.1.1"

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "sphinx.ext.napoleon",  # Parse NumPy docstrings syntax
    "sphinx.ext.duration",
    "sphinx.ext.doctest",
    "sphinx.ext.todo",
    "sphinx.ext.autodoc",
    "sphinx.ext.autosummary",
    "sphinx.ext.intersphinx",
    "sphinx_rtd_theme",
    "myst_parser",  # used to use both Markdown and RST files
    #"nbsphinx",  # use jupyter notebooks
]

source_suffix = {
    ".rst": "restructuredtext",
    ".txt": "markdown",
    ".md": "markdown",
}

intersphinx_mapping = {
    "rtd": ("https://docs.readthedocs.io/en/stable/", None),
    "python": ("https://docs.python.org/3/", None),
    "numpy": ("https://numpy.org/doc/stable/", None),
    "pandas": ("http://pandas.pydata.org/docs/", None),
    "sphinx": ("https://www.sphinx-doc.org/en/master/", None),
}
intersphinx_disabled_domains = ["std"]

# -- Options for EPUB output
epub_show_urls = "footnote"

templates_path = ["_templates"]
exclude_patterns = ["_build", "tests"]

## -- nbsphinx settings --

nbsphinx_execute = "never"  # block execution of notebooks

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "sphinx_rtd_theme"
html_static_path = ["_static"]
html_logo = "logo_pychronicles.png"
html_theme_options = {
    'logo_only': True,
    'display_version': False,
}

# disable docstring in Google style (the project uses NumPy style)
napoleon_google_docstring = False
